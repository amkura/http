import {Injectable} from '@angular/core';
import {HttpClient, HttpEventType, HttpHeaders, HttpParams} from '@angular/common/http';
import {Post} from './post.model';
import {catchError, map, tap} from 'rxjs/operators';
import {Subject,  throwError} from 'rxjs';

@Injectable({providedIn: 'root'})
export class PostsService {

  error = new Subject<string>();

  constructor(private http: HttpClient) {
  }

  createAndStorePost(title: string, content: string) {
    const postData: Post = {title, content};

    this.http
      .post<{name: string}>('https://angular-90871-default-rtdb.europe-west1.firebasedatabase.app/posts.json', postData, {
        observe: 'response' // body (default) - response data extracted; response - a fuller response data
      })
      .subscribe(responseData => {
        console.log(responseData);
      }), error => {
        this.error.next(error.message);
    };
  }

  fetchPosts() {
    let searchParams = new HttpParams();
    searchParams = searchParams.append('print', 'pretty');
    return this.http
      .get<{ [key: string]: Post}>('https://angular-90871-default-rtdb.europe-west1.firebasedatabase.app/posts.json', {
        headers: new HttpHeaders({
          'Custom-Header': 'Hello'
        }),
        params: searchParams
      })
      .pipe(
        map((responseData) => {
          const postsArray: Post[] = [];
          for(const key in responseData) {
            // making sure we're not trying to access a property of some prototype
            if(responseData.hasOwnProperty(key)) {
              postsArray.push({...responseData[key], id: key});
            }
          }
          return postsArray;
        }),
        catchError(errorRes => {
          return throwError(errorRes);
        })
      );
  }

  clearPosts() {
    return this.http
      .delete('https://angular-90871-default-rtdb.europe-west1.firebasedatabase.app/posts.json', {
        observe: 'events',
        responseType: 'text'
      })
      .pipe(
        tap(event => {
          console.log('Data: ', event);
          if(event.type === HttpEventType.Sent) {
            // you can notify that the request was sent here
          }
          if(event.type === HttpEventType.Response) {
            console.log(event.body);
          }
        })
      );
  }
}
